#!/usr/bin/env python3
# -*- coding=utf-8 -*-

import argparse
import re
import sqlite3


def my_filter_db(sample, name_file="sample.db", search_limit=10):
    with sqlite3.connect(name_file) as db_connection:
        list_of_numbers = list()

        for nums in sample:
            if re.search(r"[^0-9+]", nums):
                print(f"{nums} has some unrelated chars.\nRemove them and try again!\n")
                break
            x = db_connection.cursor().execute("""
            SELECT number FROM numbers WHERE number LIKE ? LIMIT ?
            ;""", (f"%{nums}%", search_limit)).fetchall()
            list_of_numbers.append(list(y[0] for y in x))
    for user_number, list_of_number in zip(sample, list_of_numbers):
        for number_in_list in list_of_number:
            print(f"""Match for {user_number} \
- {my_colored(number_in_list, user_number)} \
({my_get_operator(number_in_list)})""")

        print("="*15)


def my_filter_txt(sample, name_file="sample.txt", limit=10):
    with open(name_file, "r") as flist:
        lnumbers = flist.read()
    list_of_numbers = list(lnumbers.split(","))
    match_dict = dict()

    for user_number in range(len(sample)):
        if re.search(r"[^0-9+]", sample[user_number]):
            print(f"{sample[user_number]} has some unrelated chars.\nRemove them and try again!\n")
            break

        match_dict[sample[user_number]] = 0

        for number_in_list in list_of_numbers:
            cur_num = sample[user_number]

            if re.findall(fr"(\b\d*{(cur_num,cur_num[1:])['+' in cur_num]}\d*)", number_in_list):
                match_dict[sample[user_number]] += 1
                print(f"""Match for {sample[user_number]} - \
{my_colored(number_in_list, sample[user_number])} \
({my_get_operator(number_in_list)})""")
                if limit and match_dict[sample[user_number]] == limit:
                    print("="*15)
                    break
    for e, a in match_dict.items():
        print(f"For {e} founded {a}'n matches.")


def my_get_operator(numb: str):
    operators_list = {
        "0893" : "Global.ua",
        "063"  : "lifecell",
        "073"  : "lifecell",
        "093"  : "lifecell",
        "0894" : "O3 / MixNet",
        "092"  : "PEOPLEnet",
        "050"  : "Vodafone Ukraine",
        "066"  : "Vodafone Ukraine",
        "0896" : "Vodafone Ukraine",
        "095"  : "Vodafone Ukraine",
        "099"  : "Vodafone Ukraine",
        "0899" : "Велтон.Телеком",
        "0891" : "Датагруп",
        "094"  : "Інтертелеком",
        "067"  : "Kyivstar",
        "068"  : "Kyivstar",
        "0897" : "Kyivstar",
        "096"  : "Kyivstar",
        "097"  : "Kyivstar",
        "098"  : "Kyivstar",
        "0895" : "Линком-3000",
        "091"  : "ТриМоб",
        "0892" : "Укртелеком",
    }
    for o_num, o_name in operators_list.items():
        if re.findall(fr'^(\+38|38|\+|8|){o_num}', numb):
            return o_name


def my_colored(full_num: str, piece_num: str):
    return (full_num.replace(piece_num, f"\033[91m{piece_num}\x1b[0m"))


def main(sample, name_file, limit):
    if name_file[-4:] == ".txt":
        my_filter_txt(sample, name_file, limit)
    elif name_file[-3:] == ".db":
        my_filter_db(sample, name_file, limit)
    else:
        print(f"""Unsuported file format - {name_file[::-1].split(".")[0][::-1]}
-h for help.""")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="""\
        Findimg mobile numbers from given list (-f) by given sample (num)\
        """,
        epilog="""\
        Example = phone_assistant.py 123 879 24 -f sample.txt -limit 50\
        """
    )
    parser.add_argument(
        "num",
        metavar="num",
        type=str,
        nargs="+",
        help="input nums for searching in given source"
    )
    parser.add_argument(
        "-f",
        type=str,
        help="""\
        Select prefered file for parsing.
        NOTE that right now suports only .db and .txt files.
        """,
        default="sample.txt"
    )
    parser.add_argument(
        "-limit",
        type=int,
        default=10
    )
    args = parser.parse_args()
    for n in args.num:
        print(1)
        print(f"{(n,chr(10)+n)['+' in n]}")
    main(args.num, args.f, args.limit)
