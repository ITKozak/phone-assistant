# Phone Assistant
## Test project for EVO Summer Python Lab

---
## Dependencise.
1. *Python 3.6+* (due to use of [f-string](https://www.python.org/dev/peps/pep-0498/))

## Getting started.
---
This  code is tested with *Python 3.6.7* and *Python 3.7.1*.

Don`t require initial setup.

## Usage.
---
Simple usage - `python3 phone_assistant.py NUM` where NUM is searching phone number.
Full Usage `phone_assistant.py [-h] [-f F] [-limit LIMIT] num [num ...]
` where:

-h  -  Help string.

-f  -  Input file (curently suporting .db and .txt with preformated data, due to no word about that moment in example task. Sample data included in repository). Default value = sample.txt.

-limit  -  Limit of displayed results. Default value = 10

NUM  -  list of nums that you are searching in data file (You can input N numbers of serched data, but any character except +0-9 will stop execution on that exact element)


More details can be aquaired by `python3 phone_assistant.py -h`.
## Known bugs.
---
1. Due to unperfect re expressions script want search num from 0 position. Need to rework that moment.

## TODO
---
1. Refactore code.
2. Rework unsupported chars handling - I just want to skip them, not fully stoped for loop.
3. Rework re expressions - implement posibility to search from the start of the num (curently I dopnt know how to hendle + char in re expressions =( ) 
